<?php
namespace TkachInc\CardDeck;
use TkachInc\Engine\Services\Helpers\ArrayHelper;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class CombinationParse
{
	/**
	 * @param array $array
	 * @param bool $reverse
	 * @param bool $priority_last
	 * @param bool $save_key
	 */
	public static function sortCard(&$array, $reverse = false, $priority_last = false, $save_key = false)
	{
		$func = function ($first, $second) use ($reverse, $priority_last) {
			$id = CardDeck::getCardPositionByValue($first);
			$id2 = CardDeck::getCardPositionByValue($second);

			if ($id > $id2) {
				return ($reverse === false) ? 1 : -1;
			}
			if ($id < $id2) {
				return ($reverse === false) ? -1 : 1;
			}
			if ($id === $id2) {
				return ($priority_last === false) ? 1 : -1;
			}

			return 0;
		};

		if ($save_key) {
			uasort($array, $func);
		} else {
			usort($array, $func);
		}
	}

	/**
	 * @param array $combination
	 * @param string $handHighCard
	 * @param bool $returnFullData
	 * @return array
	 */
	public static function parse(Array $combination, $handHighCard = '', $returnFullData = false)
	{
		static::sortCard($combination); // Для правильного разбора комбинации нужно отсортировать комбинации
		$ranks = []; // Масив повторів рангів
		$suits = []; // Масив повторів мастей
		$straighets = []; // Масив повторів стріта
		$key = 0;
		$scores = 0;

		$first_rank = null;
		$last_rank = null;
		$first_suit = null;
		$last_suit = null;

		$high_rank = 0;

		if (!empty($handHighCard)) {
			$id = CardDeck::getCardPositionByValue($handHighCard);
			$handHighCardRank = CardDeck::getRankById($id);
			$scores += $handHighCardRank << 2;
		}

		foreach ($combination as $card) {
			if (!isset($straighets[$key])) {
				$straighets[$key] = [];
			}
			if (!isset($straighets[$key]['suits'])) {
				$straighets[$key]['suits'] = [];
			}

			$id = CardDeck::getCardPositionByValue($card);
			$rank = CardDeck::getRankById($id);
			$suit = CardDeck::getSuitById($id);
			ArrayHelper::incArray($ranks, $rank);
			ArrayHelper::incArray($suits, $suit);

			$scores += $rank;

			if ($rank > $high_rank) { // Ищем старший ранг комбинации
				$high_rank = $rank;
			}

			if ($first_rank === null) { // Запоминаем первую карту комбинации
				$first_rank = $rank;
			}

			if ($last_rank !== null) { // Проверяем стрит, когда получена последняя карта комбинации (хотя бы одна)
				if ($last_rank + 1 === $rank || ($rank === 12 && $first_rank === 0)) {
					ArrayHelper::incArray($straighets[$key], 'straight');
					ArrayHelper::incArray($straighets[$key]['suits'], $suit);
					$straighets[$key]['last_card_straight'] = $rank;
					$straighets[$key]['first_card_straight'] = $first_rank;
				} elseif ($last_rank === $rank) {
				} else { // Если карт меньше четырех в стрите, скидываем параметры стрита
					$key++;
					$straighets[$key]['last_card_straight'] = null;
					$straighets[$key]['first_card_straight'] = null;
					$straighets[$key]['suits'] = [];
					$first_rank = $rank;
				}
			} else {
				ArrayHelper::incArray($straighets[$key]['suits'], $suit);
			}

			// Запоминаем последнюю карту комбинации
			$last_rank = $rank;
		}

		foreach ($straighets as $key => $straighet) {
			foreach ($straighet['suits'] as $suit => $count) {
				if ($count >= 5) {
					$straighets[$key]['straight_flush'] = $straighets[$key]['straight'];
					$straighets[$key]['last_card_straight_flush'] = $straighets[$key]['last_card_straight'];
					$straighets[$key]['first_card_straight_flush'] = $straighets[$key]['first_card_straight'];
					$straighets[$key]['straight_flush_suit'] = $suit;
					break 2;
				}
			}
		}

		$combId = 1;
		foreach ($straighets as $straight) {
			if (isset($straight['straight_flush']) && $straight['straight_flush'] >= 4) {
				if (isset($straight['last_card_straight_flush']) && $straight['last_card_straight_flush'] === 12) {
					if ($combId < 10) {
						$combId = 10;
					}
				} else {
					if ($combId < 9) {
						$combId = 9;
					}
				}
			} elseif (isset($straight['straight']) && $straight['straight'] >= 4) {
				if ($combId < 5) {
					$combId = 5;
				}
			}
		}
		foreach ($suits as $count) {
			if ($count >= 5) {
				if ($combId < 6) {
					$combId = 6;
				}
			}
		}

		$pairs = 0;
		$sets = 0;
		foreach ($ranks as $count) {
			if ($count >= 2) {
				if ($count >= 3) {
					if ($count >= 4) {
						if ($combId < 8) {
							$combId = 8;
						}
					} else {
						if ($combId < 4) {
							$combId = 4;
						}
						$sets++;
					}
				} else {
					if ($combId < 2) {
						$combId = 2;
					}
					$pairs++;
				}
			}

			if ($sets >= 1 && $pairs >= 1 || $sets >= 2) {
				if ($combId < 7) {
					$combId = 7;
				}
			}
			if ($pairs >= 2) {
				if ($combId < 3) {
					$combId = 3;
				}
			}
		}

		// Досчитываем очки комбинации
		$scores = ($combId << 16) + ($high_rank << 2) + $scores;

		$result = ['combId' => $combId, 'scores' => $scores, 'high_rank' => $high_rank];
		if ($returnFullData === true) {
			$result['straighets'] = $straighets;
			$result['ranks'] = $ranks;
			$result['suits'] = $suits;
		}

		return $result;
	}

	/**
	 * @param          $combId
	 * @param array $combination
	 * @param CardDeck $mainDeck
	 */
	public static function removingCardsAmplifiers($combId, Array $combination, CardDeck $mainDeck)
	{
		static::sortCard($combination); // Для правильного разбора комбинации нужно отсортировать комбинации
		$ranks = []; // Масив повторів рангів
		$suits = []; // Масив повторів мастей
		$straighets = []; // Масив повторів стріта
		$key = 0;

		$scores = 0;

		$first_rank = null;
		$last_rank = null;
		$first_suit = null;
		$last_suit = null;

		$high_rank = 0;

		foreach ($combination as $card) {
			if (!isset($straighets[$key])) {
				$straighets[$key] = [];
			}

			$id = CardDeck::getCardPositionByValue($card);
			$rank = CardDeck::getRankById($id);
			$suit = CardDeck::getSuitById($id);
			ArrayHelper::incArray($ranks, $rank);
			ArrayHelper::incArray($suits, $suit);

			$scores += $rank;

			if ($rank > $high_rank) { // Ищем старший ранг комбинации
				$high_rank = $rank;
			}

			if ($first_rank === null) { // Запоминаем первую карту комбинации
				$first_rank = $rank;
				$first_suit = $suit;
			}

			if ($last_rank !== null) { // Проверяем стрит, когда получена последняя карта комбинации (хотя бы одна)
				if (($last_suit === $suit && ($last_rank + 2 === $rank || $last_rank + 1 === $rank)) ||
					($rank === 12 && $first_rank === 0 && $last_suit === $suit && $first_suit === $suit)
				) {
					ArrayHelper::incArray($straighets[$key], 'straight');
					ArrayHelper::incArray($straighets[$key], 'straight_flush');
					$straighets[$key]['last_card_straight_flush'] = $rank;
					$straighets[$key]['first_card_straight_flush'] = $first_rank;
					$straighets[$key]['last_card_straight'] = $rank;
					if ($last_rank + 2 === $rank) {
						$straighets[$key]['straight_remove_rank'] = $last_rank + 1;
					}
					$straighets[$key]['first_card_straight'] = $first_rank;
					$straighets[$key]['straight_flush_suit'] = $suit;
				} elseif ($last_rank + 2 === $rank || $last_rank + 1 === $rank || ($rank === 12 && $first_rank === 0)) {
					ArrayHelper::incArray($straighets[$key], 'straight');
					$straighets[$key]['straight_flush'] = 0;
					$straighets[$key]['last_card_straight_flush'] = null;
					$straighets[$key]['first_card_straight_flush'] = null;
					$straighets[$key]['straight_flush_suit'] = null;
					$straighets[$key]['last_card_straight'] = $rank;
					if ($last_rank + 2 === $rank) {
						$straighets[$key]['straight_remove_rank'] = $last_rank + 1;
					}
					$straighets[$key]['first_card_straight'] = $first_rank;
				} elseif ($last_rank === $rank) {
				} else { // Если карт меньше четырех в стрите, скидываем параметры стрита
					$key++;
					$straighets[$key]['last_card_straight_flush'] = null;
					$straighets[$key]['last_card_straight'] = null;
					$straighets[$key]['first_card_straight_flush'] = null;
					$straighets[$key]['first_card_straight'] = null;
					$straighets[$key]['straight_flush_suit'] = null;
					$first_rank = $rank;
					$first_suit = $suit;
				}
			}

			// Запоминаем последнюю карту комбинации
			$last_rank = $rank;
			$last_suit = $suit;
		}

		foreach ($straighets as $straight) {
			if (isset($straight['straight_flush']) && $straight['straight_flush'] >= 3) {
				if ($combId < 10 && $combId === 9) {
					$mainDeck->removeByRankAndSuit(12);
				}

				if ($combId < 9) {
					// ...
					if (isset($straight['straight_remove_rank'])) {
						$mainDeck->removeByRankAndSuit($straight['straight_remove_rank']);
					}
					if (isset($straight['first_card_straight_flush'])) {
						$mainDeck->removeByRankAndSuit($straight['first_card_straight_flush'] - 1);
					}
					if (isset($straight['last_card_straight_flush'])) {
						$mainDeck->removeByRankAndSuit($straight['last_card_straight_flush'] + 1);
					}
					if (isset($straight['straight_flush_suit']) && $combId !== 6) {
						$mainDeck->removeByRankAndSuit(null, $straight['straight_flush_suit']);
					}
				}
			}
			if (isset($straight['straight']) && $straight['straight'] >= 3) {
				if ($combId < 5) {
					// ...
					if (isset($straight['straight_remove_rank'])) {
						$mainDeck->removeByRankAndSuit($straight['straight_remove_rank']);
					}
					if (isset($straight['first_card_straight'])) {
						$mainDeck->removeByRankAndSuit($straight['first_card_straight'] - 1);
					}
					if (isset($straight['last_card_straight'])) {
						$mainDeck->removeByRankAndSuit($straight['last_card_straight'] + 1);
					}
				}
			}
		}

		foreach ($suits as $suit => $count) {
			if ($count >= 4) {
				if ($combId < 6) {
					// ...
					$mainDeck->removeByRankAndSuit(null, $suit);
				}
			}
		}

		$pairs = 0;
		$sets = 0;
		foreach ($ranks as $rank => $count) {
			if ($count >= 1) {
				if ($count >= 2) {
					if ($count >= 3) {
						if ($combId < 8) {
							// ...
							$mainDeck->removeByRankAndSuit($rank);
						}
						$sets++;
					}
					if ($combId < 4) {
						// ...
						$mainDeck->removeByRankAndSuit($rank);
					}
					$pairs++;
				}
				if ($combId < 2) {
					// ...
					$mainDeck->removeByRankAndSuit($rank);
				}
				//				$pairs++;
			}

			if (($sets >= 1 && $pairs >= 1) || ($pairs >= 2)) {
				if ($combId < 7) {
					// ...
					$mainDeck->removeByRankAndSuit($rank);
				}
			}
			if ($pairs >= 1) {
				if ($combId < 3) {
					// ...
					$mainDeck->removeByRankAndSuit($rank);
				}
			}
		}
	}
}