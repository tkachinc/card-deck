<?php
namespace TkachInc\CardDeck;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class CardDeck
{
	protected static $baseDeck = [
		'2h',
		'2s',
		'2d',
		'2c',
		'3h',
		'3s',
		'3d',
		'3c',
		'4h',
		'4s',
		'4d',
		'4c',
		'5h',
		'5s',
		'5d',
		'5c',
		'6h',
		'6s',
		'6d',
		'6c',
		'7h',
		'7s',
		'7d',
		'7c',
		'8h',
		'8s',
		'8d',
		'8c',
		'9h',
		'9s',
		'9d',
		'9c',
		'Th',
		'Ts',
		'Td',
		'Tc',
		'Jh',
		'Js',
		'Jd',
		'Jc',
		'Qh',
		'Qs',
		'Qd',
		'Qc',
		'Kh',
		'Ks',
		'Kd',
		'Kc',
		'Ah',
		'As',
		'Ad',
		'Ac',
	];
	protected $deck = [];
	protected $shuffleDeck = [];

	/**
	 * @param array $usedCards
	 */
	public function __construct($usedCards = [])
	{
		$this->deck = static::getBaseDeck();
		foreach ($usedCards as $val) {
			$key = $this->getCardPositionByValue($val);
			$this->removeById($key);
		}
	}

	/**
	 * @param      $rank
	 * @param bool $removeReturnCard
	 *
	 * @return array
	 */
	public function getAllCardsByRank($rank, $removeReturnCard = false)
	{
		$cards = [];
		for ($suit = 0; $suit <= 3; $suit++) {
			$id = $this->getCardIdByRankAndSuit($rank, $suit);
			if ($this->issetCardById($id)) {
				$cards[] = $this->getCardById($id, $removeReturnCard);
			}
		}

		return $cards;
	}

	/**
	 * @param      $suit
	 * @param bool $removeReturnCard
	 *
	 * @return array
	 */
	public function getAllCardsBySuit($suit, $removeReturnCard = false)
	{
		$cards = [];
		for ($rank = 0; $rank <= 12; $rank++) {
			$id = $this->getCardIdByRankAndSuit($rank, $suit);
			if ($this->issetCardById($id)) {
				$cards[$id] = $this->getCardById($id, $removeReturnCard);
			}
		}

		return $cards;
	}

	/**
	 * @param array $ranks
	 * @param array $suits
	 * @param bool $groups
	 * @param bool $removeReturnCard
	 *
	 * @return array
	 */
	public function getAllCardByParams(Array $ranks = [], Array $suits = [], $groups = false, $removeReturnCard = false)
	{
		$cards = [];
		foreach ($ranks as $rank) {
			foreach ($suits as $suit) {
				$id = $this->getCardIdByRankAndSuit($rank, $suit);
				if ($this->issetCardById($id)) {
					if ($groups) {
						$cards[$rank][$id] = $this->getCardById($id, $removeReturnCard);
					} else {
						$cards[$id] = $this->getCardById($id, $removeReturnCard);
					}
				}
			}
		}

		return $cards;
	}

	/**
	 * @param $id
	 *
	 * @return bool
	 */
	public function issetCardById($id)
	{
		return isset($this->deck[$id]);
	}

	/**
	 * @return array
	 */
	public static function getBaseDeck()
	{
		return static::$baseDeck;
	}

	/**
	 * @return array
	 */
	public function getDeck()
	{
		return $this->deck;
	}

	/**
	 * @return array
	 */
	public function getShuffleDeck()
	{
		return $this->shuffleDeck;
	}

	public function shuffleDeck()
	{
		$this->shuffleDeck = $this->deck;
		shuffle($this->shuffleDeck);
	}

	/**
	 * @param      $id
	 * @param bool $removeReturnCard
	 *
	 * @return mixed
	 */
	public function getCardById($id, $removeReturnCard = true)
	{
		if (empty($this->deck) || !isset($this->deck[$id])) {
			return null;
		}

		$card = $this->deck[$id];

		if ($removeReturnCard) {
			$this->removeById($id);
		}

		return $card;
	}

	/**
	 * @param bool $removeReturnCard
	 *
	 * @return null
	 */
	public function getRandomCard($removeReturnCard = true)
	{
		if (empty($this->deck)) {
			return null;
		}

		$key = array_rand($this->deck);
		$card = $this->deck[$key];

		if ($removeReturnCard) {
			$this->removeById($key);
		}

		return $card;
	}

	/**
	 * @return mixed
	 */
	public function getTopCard()
	{
		if (empty($this->shuffleDeck)) {
			return null;
		}

		return array_pop($this->shuffleDeck);
	}

	/**
	 * @param $id
	 *
	 * @return mixed
	 */
	public static function getCardByPosition($id)
	{
		return isset(static::$baseDeck[$id]) ? static::$baseDeck[$id] : null;
	}

	/**
	 * @param $card
	 *
	 * @return mixed
	 */
	public static function getCardPositionByValue($card)
	{
		return array_search($card, static::$baseDeck);
	}

	/**
	 * @param $id
	 *
	 * @return int
	 */
	public static function getSuitById($id)
	{
		return (int)$id % 4;
	}

	/**
	 * @param $id
	 *
	 * @return int
	 */
	public static function getRankById($id)
	{
		return (int)floor($id / 4);
	}

	/**
	 * @param $rank
	 * @param $suit
	 *
	 * @return mixed
	 */
	public function getCardIdByRankAndSuit($rank, $suit)
	{
		$id = ($rank * 4) + $suit;

		return $id;
	}

	/**
	 * @param $id
	 */
	public function removeById($id)
	{
		unset($this->deck[$id]);
	}

	/**
	 * @param $id
	 */
	public function restoreById($id)
	{
		if (isset(static::$baseDeck[$id])) {
			$this->deck[$id] = static::$baseDeck[$id];
		}
	}

	/**
	 * @param null $rank
	 * @param null $suit
	 */
	public function removeByRankAndSuit($rank = null, $suit = null)
	{
		if (isset($rank) && !isset($suit)) {
			$this->getAllCardsByRank($rank, true);
		} elseif (!isset($rank) && isset($suit)) {
			$this->getAllCardsBySuit($suit, true);
		} elseif (isset($rank) && isset($suit)) {
			$id = $this->getCardIdByRankAndSuit($rank, $suit);
			$this->removeById($id);
		}
	}

	/**
	 * @param array $cards
	 * @param CardDeck $deck
	 * @param          $rank
	 *
	 * @return array
	 */
	public static function getCardInCardsByRank(Array $cards, CardDeck $deck, $rank)
	{
		$rankCards = $deck->getAllCardsByRank($rank, false);

		return array_intersect($cards, $rankCards);
	}

	/**
	 * @param array $cards
	 * @param CardDeck $deck
	 * @param          $suit
	 *
	 * @return array
	 */
	public static function getCardInCardsBySuit(Array $cards, CardDeck $deck, $suit)
	{
		$rankSuits = $deck->getAllCardsBySuit($suit, false);

		return array_intersect($cards, $rankSuits);
	}

	/**
	 * @param      $rank
	 * @param bool $removeReturnCard
	 *
	 * @return mixed
	 */
	public function getRandomCardByRank($rank, $removeReturnCard = false)
	{
		$result = $this->getAllCardByParams([$rank], [0, 1, 2, 3], false, false);
		$id = null;
		if (is_array($result)) {
			$id = array_rand($result);
			if ($removeReturnCard === true) {
				$this->removeById($id);
			}
		}

		return $id;
	}

	/**
	 * @param      $suit
	 * @param bool $removeReturnCard
	 *
	 * @return mixed
	 */
	public function getRandomCardBySuit($suit, $removeReturnCard = false)
	{
		$result = $this->getAllCardByParams([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], [$suit], false, false);
		$id = null;
		if (is_array($result)) {
			$id = array_rand($result);
			if ($removeReturnCard === true) {
				$this->removeById($id);
			}
		}

		return $id;
	}
}